from Functions import *
from Generate_field import *

"""
Player
1=white, -1=black

Board Channels
0: score field
1: white が占有してるマス
2: black が占有してるマス
3: white_1 の現在地
4: white_2 の現在地
5: black_1 の現在地
6: black_2 の現在地
7: 残りターン数
"""


class Board:
    def __init__(self, score_field=None, white_1=None, white_2=None, turn=40, board=None):
        # setup board

        # indicate of action
        self.white_action, self.black_action = None, None

        # Game.py上での処理にこのクラスを使うときはフィールド生成する必要はないのでそれらの処理を飛ばす
        if board is not None:
            self.board = board
            return

        # Generate Field
        if score_field is None and white_1 is None and white_2 is None:
            score_field = generate_field()
            white_1, white_2 = generate_init_position(score_field.shape)

        # Genetate black position
        black_1, black_2 = generate_black_position(white_1, white_2)

        # plot init position
        agent_fields = np.zeros(score_field.shape + (4,), dtype=np.int)
        agent_fields[:, :, 0][white_1[0]][white_1[1]] = 1
        agent_fields[:, :, 1][white_2[0]][white_2[1]] = 1
        agent_fields[:, :, 2][black_1[0]][black_1[1]] = 1
        agent_fields[:, :, 3][black_2[0]][black_2[1]] = 1

        # 初期位置のマスを占領
        occupation_fields = np.zeros(score_field.shape + (2,), dtype=np.int)
        occupation_fields[:, :, 0][white_1[0]][white_1[1]] = 1
        occupation_fields[:, :, 0][white_2[0]][white_2[1]] = 1
        occupation_fields[:, :, 1][black_1[0]][black_1[1]] = 1
        occupation_fields[:, :, 1][black_2[0]][black_2[1]] = 1

        # 残りターン数
        rest_of_turn = np.zeros(score_field.shape, dtype=np.int)
        rest_of_turn[:] = turn

        # すべてのField要素を結合
        self.board = np.dstack((score_field, occupation_fields, agent_fields, rest_of_turn))

    def count_diff(self, player):
        """ return difference of score
        1 or -1
        """
        my_score = self.get_score(player)
        opponent_score = self.get_score(-player)

        return my_score - opponent_score

    # エージェントの座標を返す
    def get_agent_position(self, player_num, agent_num):
        """
        :param player_num: 1 or -1
        :param agent_num:  1 or 2
        :return: current agent position (row, column)
        """
        agent_channel = get_agent_channel(player_num, agent_num)
        return get_one_in_2dzeros(self.board[:, :, agent_channel])

    # get agent current position and action
    def get_agent_directory(self):
        return {
            "white_1": {
                "agent_num": 1,
                "action_num": self.white_action[0][0] if self.white_action else None,
                "current_position": get_one_in_2dzeros(self.board[:, :, 3]),
                "direction": self.white_action[0][1] if self.white_action else None
            },
            "white_2": {
                "agent_num": 2,
                "action_num": self.white_action[1][0] if self.white_action else None,
                "current_position": get_one_in_2dzeros(self.board[:, :, 4]),
                "direction": self.white_action[1][1] if self.white_action else None
            },
            "black_1": {
                "agent_num": 1,
                "action_num": self.black_action[0][0] if self.black_action else None,
                "current_position": get_one_in_2dzeros(self.board[:, :, 5]),
                "direction": self.black_action[0][1] if self.black_action else None
            },
            "black_2": {
                "agent_num": 2,
                "action_num": self.black_action[1][0] if self.black_action else None,
                "current_position": get_one_in_2dzeros(self.board[:, :, 6]),
                "direction": self.black_action[1][1] if self.black_action else None
            },
        }

    # エージェントの座標を更新する
    def update_agent_position(self, player_num, agent_num, direction):
        agent_position = self.get_agent_position(player_num, agent_num)
        updated_agent_position = addition_of_tuple(agent_position, direction)

        agent_channel = get_agent_channel(player_num, agent_num)
        self.board[:, :, agent_channel] = np.zeros(self.board.shape)
        self.board[:, :, agent_channel][updated_agent_position] = 1

    # 移動
    def move(self, player_num=1, agent_num=1, direction=(-1, 1)):
        """
        :param player_num: player number 1 or -1
        :param agent_num: agent number 1 or 2
        :param direction: direction of action
        :after process: Moved the agent and occupied a tile or
                        The agent can't move if the direction of
                         a move is to the opponent tile
        """
        # エージェントの座標を取得
        agent = self.get_agent_position(player_num, agent_num)
        # 現在エージェント座標+移動方向
        apply_position = addition_of_tuple(agent, direction)

        # get opponent channel of self.board
        opponent_channel = 2 if player_num == 1 else 1
        # 移動先が相手のマスなら移動しない
        if self.board[:, :, opponent_channel][apply_position] == 1:
            return

        # get current player channel of self.board
        player_channel = 1 if player_num == 1 else 2
        # update occupation tiles
        self.board[:, :, player_channel][apply_position] = 1  # 移動先のタイルを支配する

        # update array of agent position
        self.update_agent_position(player_num, agent_num, direction)

    # タイル除去
    def delete_tile(self, player_num=1, agent_num=1, direction=(-1, 1)):
        agent = self.get_agent_position(player_num, agent_num)
        tile = addition_of_tuple(agent, direction)
        if player_num == 1:
            self.board[:, :, 2][tile] = 0
        else:
            self.board[:, :, 1][tile] = 0

    # コマンドナンバーで行動を分岐する関数
    def branch_of_action(self, player_num=1, agent_num=1, action_num=1, direction=(-1, 1)):
        if action_num == 1:
            self.move(player_num, agent_num, direction)
        else:
            self.delete_tile(player_num, agent_num, direction)

    def take_action(self, player_num, action):
        """
        :param player_num: 1 or -1
        :param action: ((actionNum, (row,column)),(actionNum,(row,column)))
                        You can know detail if you look README.md.
        """
        # 行動意思表示を行う
        if player_num == 1:
            self.white_action = action
        else:
            self.black_action = action

        # 2者のプレイヤーが意思表示を行い終わってないなら行動しない
        if (self.white_action is None) or (self.black_action is None):
            return

        # get agent actions
        agent_actions = self.get_agent_directory()

        # 整合性のチェック
        # 整合性の取れたエージェントのみ行動。それ以外はそのまま
        for agent in agent_actions.keys():
            apply_position = addition_of_tuple(agent_actions[agent]["current_position"],
                                               agent_actions[agent]["direction"])

            # 行動先がarrayの範囲外
            if apply_position[0] < 0 or apply_position[1] < 0 or apply_position[0] > 11 or apply_position[1] > 11:
                # 行動先が12*12の範囲外なら移動しない
                apply_position = agent_actions[agent]["current_position"]
                agent_actions[agent]["direction"] = (0, 0)

            # 行動先がフィールドの範囲外なら行動せずにその場に留まるようにする
            if self.board[:, :, 7][apply_position] == 0:
                agent_actions[agent]["direction"] = (0, 0)

            # 移動先が敵マスだったら移動しない
            if agent_actions[agent]["action_num"] == 1:
                if "white" in agent:
                    if self.board[:, :, 2][apply_position] == 1:
                        agent_actions[agent]["direction"] = (0, 0)
                else:
                    if self.board[:, :, 1][apply_position] == 1:
                        agent_actions[agent]["direction"] = (0, 0)

        # 実際に行動する

        # 全エージェント行動後の座標リスト
        apply_positions = [
            addition_of_tuple(agent_actions[agent]["current_position"], agent_actions[agent]["direction"])
            for agent in agent_actions]

        for agent, agent_list_num in zip(agent_actions, range(4)):
            copied_position = apply_positions.copy()

            # ループ対象となるエージェントの座標を取り出し、消す
            target_agent_position = copied_position[agent_list_num]
            del copied_position[agent_list_num]

            # 他のエージェント行動先が被った場合行動しない
            if target_agent_position in copied_position:
                continue

            if "white" in agent:
                player_num = 1
            else:
                player_num = -1
            self.branch_of_action(player_num, agent_actions[agent]["agent_num"], agent_actions[agent]["action_num"],
                                  agent_actions[agent]["direction"])

        # turn のチャンネルから数字を-1
        cur_turn = self.board[:, :, 8][0, 0]
        self.board[:, :, 7][:] = cur_turn - 1

        # アクションのクラス変数をリセットする
        self.white_action = None
        self.black_action = None

    # 価値のある行動に対応したインデックスは1のリストを返す
    def get_valid_actions(self, player_num):
        # 基本となるarray
        actions = get_action_patterns()
        valid_actions = np.ones(256)

        # エージェントの座標を取得
        agent_1 = self.get_agent_position(player_num, 1)
        agent_2 = self.get_agent_position(player_num, 2)

        # 敵エージェントの座標を取得
        enemy_1 = self.get_agent_position(-player_num, 1)
        enemy_2 = self.get_agent_position(-player_num, 2)

        # 敵の占領状況チャンネルを取得
        enemy_channel = 2 if player_num == 1 else 1
        enemy_board = self.board[:, :, enemy_channel]

        max_rows, max_columns = self.board.shape[0], self.board.shape[1]

        for i, action in enumerate(actions):
            for agent_num, agent in enumerate([agent_1, agent_2]):

                # 移動
                if action[agent_num][0] == 1:
                    # 行動後の座標
                    apply_position = addition_of_tuple(agent, action[agent_num][1])
                    # 行動先のマスが占有マスじゃなければ行動の価値あり
                    # 他のエージェントがいたら価値なし
                    # フィールドの範囲外への行動は価値なし
                    if apply_position[0] < 0 or apply_position[1] < 0 or \
                            apply_position[0] > max_rows or apply_position[1] > max_columns or \
                            enemy_board[apply_position] == 1 or \
                            apply_position == enemy_1 or apply_position == enemy_2:
                        valid_actions[i] = 0

                # タイル除去
                else:
                    # 行動後の座標
                    apply_position = addition_of_tuple(agent, action[agent_num][1])
                    # 行動先のマスが敵の占有マスなら行動
                    # 他のエージェントがいたら価値なし
                    # フィールドの範囲外への行動は価値なし
                    if apply_position[0] < 0 or apply_position[1] < 0 or \
                            apply_position[0] > max_rows or apply_position[1] > max_columns or \
                            enemy_board[apply_position] == 0 or \
                            apply_position == enemy_1 or apply_position == enemy_2:
                        valid_actions[i] = 0

        return valid_actions

    # 現在の点数を返す
    # player = 1 or -1
    def get_score(self, player=1):
        # Player占有マスのチャンネルを取り出す
        if player == 1:
            possession_of_tile = self.board[:, :, 1][:self.board.shape[0], :self.board.shape[1]]
        else:
            possession_of_tile = self.board[:, :, 2][:self.board.shape[0], :self.board.shape[0]]

        # 12*12のBoardから元の盤面を取り出す
        score_field = self.board[:, :, 0][:self.board.shape[0], :self.board.shape[1]]

        # 占有してるマスの座標取得
        i, j = np.where(possession_of_tile == 1)

        # タイルポイント
        tile_score = [score_field[i][j] for i, j in zip(i, j)]
        tile_score = np.sum(tile_score)

        # 領域ポイント
        max_rows, max_columns = self.board.shape[0], self.board.shape[1]
        # 領域ポイントの探索用のマスクを作成
        mask = create_mask(possession_of_tile, max_rows - 1, max_columns - 1)
        # 領域ポイント集計
        area_tile = np.where(mask == 0)
        area_score = [abs(score_field[i][j]) for i, j in zip(area_tile[0], area_tile[1])]
        area_score = np.sum(area_score)

        # print("player{} tile score: {}".format(player, tile_score))
        # print("player{} area score: {}".format(player, area_score))

        # 占有ポイントとエリアポイントの合計を返す
        return tile_score + area_score
