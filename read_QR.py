# -*- coding: utf-8 -*-
"""
Created on Tue Oct 7 11:41:42 2018

@author: Caihao.Cui

https://github.com/cuicaihao/Webcam_QR_Detector/blob/master/Lab_02_QR_Bar_Code_Detector_Webcam.ipynb
"""
from __future__ import print_function

import pyzbar.pyzbar as pyzbar
import numpy as np
import cv2
import time


def read_QR():
    # get the webcam:
    # ここはPCによって値を変える
    cap = cv2.VideoCapture(1)

    cap.set(3, 640)
    cap.set(4, 480)
    # 160.0 x 120.0
    # 176.0 x 144.0
    # 320.0 x 240.0
    # 352.0 x 288.0
    # 640.0 x 480.0
    # 1024.0 x 768.0
    # 1280.0 x 1024.0
    time.sleep(2)

    def decode(im):
        # Find barcodes and QR codes
        decodedObjects = pyzbar.decode(im)
        # Print results
        for obj in decodedObjects:
            print('Type : ', obj.type)
            print('Data : ', obj.data, '\n')
        return decodedObjects

    font = cv2.FONT_HERSHEY_SIMPLEX

    while (cap.isOpened()):
        # Capture frame-by-frame
        ret, frame = cap.read()
        # Our operations on the frame come here
        im = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        decodedObjects = decode(im)

        for decodedObject in decodedObjects:
            points = decodedObject.polygon

            # If the points do not form a quad, find convex hull
            if len(points) > 4:
                hull = cv2.convexHull(np.array([point for point in points], dtype=np.float32))
                hull = list(map(tuple, np.squeeze(hull)))
            else:
                hull = points

            # Number of points in the convex hull
            n = len(hull)
            # Draw the convext hull
            for j in range(0, n):
                cv2.line(frame, hull[j], hull[(j + 1) % n], (255, 0, 0), 3)

            x = decodedObject.rect.left
            y = decodedObject.rect.top

            print(x, y)

            print('Type : ', decodedObject.type)
            print('Data : ', decodedObject.data, '\n')

            barCode = str(decodedObject.data)
            cv2.putText(frame, barCode, (x, y), font, 1, (0, 255, 255), 2, cv2.LINE_AA)

            # QRを読み込んだら返す
            if barCode:
                cap.release()
                cv2.destroyAllWindows()
                return decodedObject.data

        # Display the resulting frame
        cv2.imshow('frame', frame)
        key = cv2.waitKey(1)
        if key & 0xFF == ord('q'):
            break
        elif key & 0xFF == ord('s'):  # wait for 's' key to save
            cv2.imwrite('Capture.png', frame)

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    content = read_QR()
    print(content)

    # csvに書き込むための前処理
    content = content.decode("utf-8")
    content = content.split(":")
    content_list = []
    for row in content:
        content_list.append(row.split(" "))

    # csvファイル書き込み
    import csv
    with open("QR_Field.csv", "w") as f:
        writer = csv.writer(f, lineterminator='\n')  # 改行コード（\n）を指定しておく
        for row in content_list:
            # 空文字が含まれていた場合は消す
            if "" in row:
                row.remove("")

            # 空文字のみの場合は書き込まずスキップする
            if not row:
                continue

            writer.writerow(row)

