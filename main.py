import tkinter as tk

from Functions import *
from GameLogic import Board


class App(tk.Tk):
    def __init__(self):
        super(App, self).__init__()
        self.title("procon_2018")
        self.resizable(0, 0)
        self.configure(bg="white")

        # Board作成
        # field_shape, score_field, me_1, me_2 = get_field_from_qr()
        # 渡された座標は一番小さい座標が(1,1)なのでそれをプログラムから扱いやすいように(0,0)にする
        # me_1 = subtraction_of_tuple(me_1, (1, 1))
        # me_2 = subtraction_of_tuple(me_2, (1, 1))
        #
        # self.score_field = score_field
        # self.field_shape = field_shape
        # turn = input("ターン数を入力: ")
        # turn = 120
        # self.game = Board(score_field, me_1, me_2, turn)
        # TODO: デバッグ用
        self.game = Board()
        self.score_field = self.game.board[:, :, 0]
        self.field_shape = self.game.board.shape

        self.set_board()
        self.set_player()
        self.board_score()

    def set_board(self):
        self.board = tk.Canvas(self, bg="white", width=550, height=550)
        self.board.pack(pady=(30, 0))
        # 基本となるフィールド
        self.boardinfo1 = np.zeros(self.field_shape)
        # tagからクリックされたtagの位置を得る
        self.tag2pos = {}
        # １次元の座標からtagを得る
        self.z2tag = {}
        # 長方形を配置する。
        for i, y in zip(range(self.field_shape[0]), range(15, 40 * self.field_shape[0], 40)):
            for j, x in zip(range(self.field_shape[1]), range(15, 40 * self.field_shape[1], 40)):
                pos = x, y, x + 40, y + 40
                tag = (i, j)
                self.tag2pos[tag] = pos
                self.board.create_rectangle(*pos, fill="white", tags=tag)
                self.z2tag[tag] = tag

    def set_player(self):

        tag1 = self.game.get_agent_position(1, 1)
        tag2 = self.game.get_agent_position(1, 2)
        tag3 = self.game.get_agent_position(-1, 1)
        tag4 = self.game.get_agent_position(-1, 2)

        # 緑チーム生成
        self.board.create_oval(*self.tag2pos[tag1], fill="green", tags=tag1)
        self.boardinfo1[tag1[0], tag1[1]] = 1

        self.board.create_oval(*self.tag2pos[tag2], fill="green", tags=tag2)
        self.boardinfo1[tag2[0], tag2[1]] = 1

        # 赤チーム生成
        self.board.create_oval(*self.tag2pos[tag3], fill="orange", tags=tag3)
        self.boardinfo1[tag4[0], tag3[1]] = 2

        self.board.create_oval(*self.tag2pos[tag4], fill="orange", tags=tag4)
        self.boardinfo1[tag4[0], tag4[1]] = 2

    def board_score(self):
        for i, y in zip(range(self.field_shape[0]), range(15, 40 * self.field_shape[0], 40)):
            for j, x in zip(range(self.field_shape[1]), range(15, 40 * self.field_shape[1], 40)):
                self.board.create_text(x + 20, y + 20, text=self.score_field[i, j])

    def run(self):
        self.mainloop()


if __name__ == "__main__":
    app = App()
    app.run()
